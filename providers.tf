#############################################################################
# TERRAFORM CONFIG
#############################################################################

terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3" 
    }

    random = {
      source  = "hashicorp/random"
      version = "3.1.0"
    }
  }

  required_version = "~> 1.8"
}

#############################################################################
# DATA
#############################################################################

data "azurerm_subscription" "current" {}

#############################################################################
# PROVIDERS
#############################################################################

provider "azurerm" {

  #  Ces informations sont à mettre si la connextion n'est pas établit avec Powershell (Azure CLI (Az login)) il seront rempli automatiquement si Az login 
  #  subscription_id = "9369f641-1c6b-450d-8a04-33fb99e661e6"
  #  tenant_id = "0c84d927-8f7c-404c-bbe0-0f49f2e811d7"
  #  client_id = ""
  #  client_secret = ""

  features {}
}

provider "azuread" {

}