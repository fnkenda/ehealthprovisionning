resource "azurerm_resource_group" "ehealth" {
  name     = var.resource_group_name
  location = var.location
}

# resource "azurerm_resource_group" "ehealth-sec" {
#   name     = var.sec_resource_group_name
#   location = var.location

#   tags = {
#     environment = var.security_environment_name
#   }
# }


# CONTROL NODE

module "vnet-controlnode" {
  source              = "Azure/vnet/azurerm"
  version             = "~> 4"

  use_for_each = var.use_for_each
  vnet_location = var.location

  resource_group_name = azurerm_resource_group.ehealth.name
  vnet_name           = var.vnet-controlnode-name
  address_space       = [var.vnet_cidr_range_controlnode]
  subnet_prefixes     = var.subnet_prefixes_controlnode
  subnet_names        = var.subnet_names_controlnode
  nsg_ids             = {}

  tags = {
    environment = "all"
    # costcenter  = var.costcenter
  }

  depends_on = [azurerm_resource_group.ehealth]
}

# DEVELOPMENT (DEV)

module "vnet-dev" {
  source              = "Azure/vnet/azurerm"
  version             = "~> 4"

  use_for_each = var.use_for_each
  vnet_location = var.location

  resource_group_name = azurerm_resource_group.ehealth.name
  vnet_name           = var.vnet-dev-name
  address_space       = [var.vnet_cidr_range_dev]
  subnet_prefixes     = var.subnet_prefixes_dev
  subnet_names        = var.subnet_names_dev
  nsg_ids             = {}

  tags = {
    environment = "dev"
    # costcenter  = var.costcenter
  }

  depends_on = [azurerm_resource_group.ehealth]
}

# USER ACCCEPTANCE TESTING (UAT)

module "vnet-uat" {
  source              = "Azure/vnet/azurerm"
  version             = "~> 4"

  use_for_each = var.use_for_each
  vnet_location = var.location

  resource_group_name = azurerm_resource_group.ehealth.name
  vnet_name           = var.vnet-uat-name
  address_space       = [var.vnet_cidr_range_uat]
  subnet_prefixes     = var.subnet_prefixes_uat
  subnet_names        = var.subnet_names_uat
  nsg_ids             = {}

  tags = {
    environment = "uat"
    # costcenter  = var.costcenter
  }

  depends_on = [azurerm_resource_group.ehealth]
}

# PRODUCTION (PROD)

module "vnet-prod" {
  source              = "Azure/vnet/azurerm"
  version             = "~> 4"

  use_for_each = var.use_for_each
  vnet_location = var.location

  resource_group_name = azurerm_resource_group.ehealth.name
  vnet_name           = var.vnet-prod-name
  address_space       = [var.vnet_cidr_range_prod]
  subnet_prefixes     = var.subnet_prefixes_prod
  subnet_names        = var.subnet_names_prod
  nsg_ids             = {}

  tags = {
    environment = "prod"
    # costcenter  = var.costcenter
  }

  depends_on = [azurerm_resource_group.ehealth]
}


############################################################################
# PEERING
############################################################################

//data "azurerm_subscription" "current" {}

resource "random_password" "vnet_peering" {
  length  = 16
  special = true
}

resource "azuread_application" "vnet_peering" {
  display_name = "vnet-peer"
}

resource "azuread_service_principal" "vnet_peering" {
  application_id = azuread_application.vnet_peering.application_id
}

resource "azuread_service_principal_password" "vnet_peering" {
  service_principal_id = azuread_service_principal.vnet_peering.id
  //value                = random_password.vnet_peering.result
  end_date_relative    = "17520h"
}

provider "azurerm" {
  alias                       = "peering"
  subscription_id             = data.azurerm_subscription.current.subscription_id
  client_id                   = azuread_service_principal.vnet_peering.application_id 
  tenant_id                   = data.azurerm_subscription.current.tenant_id
  skip_provider_registration  = true
    features {}
}

resource "azurerm_role_definition" "vnet-peering" {
  name  = "allow-vnet-peer"
  scope = data.azurerm_subscription.current.id

  permissions {
    actions     = ["Microsoft.Network/virtualNetworks/virtualNetworkPeerings/write", "Microsoft.Network/virtualNetworks/peer/action", "Microsoft.Network/virtualNetworks/virtualNetworkPeerings/read", "Microsoft.Network/virtualNetworks/virtualNetworkPeerings/delete"]
    not_actions = []
  }

  assignable_scopes = [
    data.azurerm_subscription.current.id,
  ]
}

resource "azurerm_role_assignment" "vnetcontrolnode" {
  scope              =  module.vnet-controlnode.vnet_id  //var.vnet_controlnode_id //module.vnet-controlnode.vnet_id 
  role_definition_id = azurerm_role_definition.vnet-peering.role_definition_resource_id
  principal_id       = azuread_service_principal.vnet_peering.id  //var.sec_principal_id
}

############################### ctrl-dev

resource "azurerm_virtual_network_peering" "ctrldev" {
  name                      = "ctrl_2_dev"
  resource_group_name       = var.resource_group_name
  virtual_network_name      = var.vnet-controlnode-name  
  remote_virtual_network_id = module.vnet-dev.vnet_id
  provider                  = azurerm.peering

  depends_on = [azurerm_role_assignment.vnetcontrolnode]
}

resource "azurerm_virtual_network_peering" "devctrl" {
  name                      = "dev_2_ctrl"
  resource_group_name       = var.resource_group_name
  virtual_network_name      = var.vnet-dev-name
  remote_virtual_network_id = module.vnet-controlnode.vnet_id
  provider                  = azurerm.peering //azurerm.security

  depends_on = [azurerm_role_assignment.vnetcontrolnode]
}

############################### ctrl-uat

resource "azurerm_virtual_network_peering" "ctrluat" {
  name                      = "ctrl_2_uat"
  resource_group_name       = var.resource_group_name
  virtual_network_name      = var.vnet-controlnode-name
  remote_virtual_network_id = module.vnet-uat.vnet_id
  provider                  = azurerm.peering

  depends_on = [azurerm_role_assignment.vnetcontrolnode]
}

resource "azurerm_virtual_network_peering" "uatctrl" {
  name                      = "uat_2_ctrl"
  resource_group_name       = var.resource_group_name
  virtual_network_name      = var.vnet-uat-name
  remote_virtual_network_id = module.vnet-controlnode.vnet_id
  provider                  = azurerm.peering  //azurerm.security

  depends_on = [azurerm_role_assignment.vnetcontrolnode]
}

############################### ctrl-prod

resource "azurerm_virtual_network_peering" "ctrlprod" {
  name                      = "ctrl_2_prod"
  resource_group_name       = var.resource_group_name
  virtual_network_name      = var.vnet-controlnode-name
  remote_virtual_network_id = module.vnet-prod.vnet_id
  provider                  = azurerm.peering

  depends_on = [azurerm_role_assignment.vnetcontrolnode]
}

resource "azurerm_virtual_network_peering" "prodctrl" {
  name                      = "prod_2_ctrl"
  resource_group_name       = var.resource_group_name
  virtual_network_name      = var.vnet-prod-name
  remote_virtual_network_id = module.vnet-controlnode.vnet_id
  provider                  = azurerm.peering //azurerm.security

  depends_on = [azurerm_role_assignment.vnetcontrolnode]
}


##############################################################################
# SECURITY (SEC)
##############################################################################

# module "vnet-sec" {
#   source              = "Azure/vnet/azurerm"
#   version             = "~> 4"

#   use_for_each = var.use_for_each
#   vnet_location = var.location

#   resource_group_name = azurerm_resource_group.ehealth-sec.name
#   vnet_name           = var.vnet-sec-name
#   address_space       = [var.vnet_cidr_range_sec]
#   subnet_prefixes     = var.subnet_prefixes_sec
#   subnet_names        = var.subnet_names_sec
#   nsg_ids             = {}

#   tags = {
#     environment = var.security_environment_name
#     # costcenter  = var.costcenter
#   }

#   depends_on = [azurerm_resource_group.ehealth-sec]
# }

## AZURE AD SP (SERVICE PRINCIPAL) ##

# resource "random_password" "vnet_peering" {
#   length  = 16
#   special = true
# }

# resource "azuread_application" "vnet_peering" {
#   display_name = "vnet-peer"
# }

# resource "azuread_service_principal" "vnet_peering" {
#   application_id = azuread_application.vnet_peering.application_id
# }

# resource "azuread_service_principal_password" "vnet_peering" {
#   service_principal_id = azuread_service_principal.vnet_peering.id
#   //value                = random_password.vnet_peering.result
#   end_date_relative    = "17520h"
# }

# resource "azurerm_role_definition" "vnet-peering" {
#   name  = "allow-vnet-peering"
#   scope = data.azurerm_subscription.current.id

#   permissions {
#     actions     = ["Microsoft.Network/virtualNetworks/virtualNetworkPeerings/write", "Microsoft.Network/virtualNetworks/peer/action", "Microsoft.Network/virtualNetworks/virtualNetworkPeerings/read", "Microsoft.Network/virtualNetworks/virtualNetworkPeerings/delete"]
#     not_actions = []
#   }

#   assignable_scopes = [
#     data.azurerm_subscription.current.id,
#   ]
# }

# resource "azurerm_role_assignment" "vnet" {
#   scope              = module.vnet-sec.vnet_id
#   role_definition_id = azurerm_role_definition.vnet-peering.role_definition_resource_id
#   principal_id       = azuread_service_principal.vnet_peering.id
# }

#############################################################################
# FILE OUTPUT
#############################################################################

# resource "local_file" "windows" {
#   filename = "${path.module}/windows-next-step.txt"
#   content  = <<EOF
# $env:TF_VAR_sec_vnet_id="${module.vnet-sec.vnet_id}"

# $env:TF_VAR_sec_vnet_name="${module.vnet-sec.vnet_name}"
# $env:TF_VAR_sec_sub_id="${data.azurerm_subscription.current.subscription_id}"
# $env:TF_VAR_sec_client_id="${azuread_service_principal.vnet_peering.application_id}"
# $env:TF_VAR_sec_principal_id="${azuread_service_principal.vnet_peering.id}"
# $env:TF_VAR_sec_client_secret="${random_password.vnet_peering.result}"
# $env:TF_VAR_sec_resource_group="${var.sec_resource_group_name}"
# $env:TF_VAR_sec_tenant_id="${data.azurerm_subscription.current.tenant_id}"

# $env:TF_VAR_resource_group="${var.resource_group_name}"

#   EOF
# }

# resource "local_file" "linux" {
#   filename = "${path.module}/next-step.txt"
#   content  = <<EOF
# export TF_VAR_sec_vnet_id=${module.vnet-sec.vnet_id}

# export TF_VAR_sec_vnet_name=${module.vnet-sec.vnet_name}
# export TF_VAR_sec_sub_id=${data.azurerm_subscription.current.subscription_id}
# export TF_VAR_sec_client_id=${azuread_service_principal.vnet_peering.application_id}
# export TF_VAR_sec_principal_id=${azuread_service_principal.vnet_peering.id}
# export TF_VAR_sec_client_secret='${random_password.vnet_peering.result}'
# export TF_VAR_sec_resource_group=${var.sec_resource_group_name}
# export TF_VAR_sec_tenant_id=${data.azurerm_subscription.current.tenant_id}

#   EOF
# }

