# CTRL

resource "azurerm_network_security_group" "ehealth-ctrl-sec" {
  name                = "ehealthSecurityGroupCtrl"
  resource_group_name = azurerm_resource_group.ehealth.name    
  location            = azurerm_resource_group.ehealth.location

  security_rule {
    name                       = "ssh-sec"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

security_rule {
    name                       = "jenkins-sec"
    priority                   = 110
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "8080"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
    environment = "All"
  }
}

resource "azurerm_network_interface" "ehealth-ctrl-nic" {
  name                = "ehealth-ctrl-nic"
  resource_group_name = azurerm_resource_group.ehealth.name
  location            = azurerm_resource_group.ehealth.location

  ip_configuration {
    name                          = "ehealth-ctrl-nic-config"
    subnet_id                     = module.vnet-controlnode.vnet_subnets[0]  //azurerm_subnet.xxxxxxxx.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.ehealth-ctrl-public-ip.id  
  }
}

resource "azurerm_public_ip" "ehealth-ctrl-public-ip" {
  name                = var.public_ip_ctrl
  resource_group_name = azurerm_resource_group.ehealth.name
  location            = azurerm_resource_group.ehealth.location
  allocation_method   = "Dynamic"

  tags = {
    envireonment = "all"
  }
}

data "azurerm_public_ip" "ehealth-ctrl-public-ip" {
  name                = azurerm_public_ip.ehealth-ctrl-public-ip.name
  resource_group_name = azurerm_linux_virtual_machine.linux-control-node.resource_group_name
}

resource "random_id" "randomid-ctrl" {
  keepers = {
    resource_group = azurerm_resource_group.ehealth.name
  }

  byte_length = 8
}

resource "azurerm_storage_account" "storage-ctrl" {
  name                     = "diag${random_id.randomid-ctrl.hex}"
  resource_group_name      = azurerm_resource_group.ehealth.name
  location                 = azurerm_resource_group.ehealth.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "tls_private_key" "ehealth-ctrl-ssh" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "export_private_ctrl_key" {
  content    = tls_private_key.ehealth-ctrl-ssh.private_key_pem 
  filename   = "id_rsa_ctrl.pem"
}

resource "local_file" "export_public_ctrl_key" {
  content    = tls_private_key.ehealth-ctrl-ssh.public_key_openssh
  filename   = "public_ctrl.pub"
}


resource "azurerm_network_interface_security_group_association" "ehealth_sga_ctrl" {
  network_interface_id      = azurerm_network_interface.ehealth-ctrl-nic.id  
  network_security_group_id = azurerm_network_security_group.ehealth-ctrl-sec.id
}

resource "azurerm_linux_virtual_machine" "linux-control-node" {
  name                = var.linux_virtual_machine_control_node_name
  resource_group_name = azurerm_resource_group.ehealth.name  
  location            = azurerm_resource_group.ehealth.location
  size                = "Standard_DS1_V2"

  computer_name                   = "ehealth-ctrl-node"
  admin_username                  = "azureuser"
  admin_password                  = "p@ssw0rd"
  disable_password_authentication = true

  admin_ssh_key {
    username   = "azureuser"
    public_key = tls_private_key.ehealth-ctrl-ssh.public_key_openssh 
  }

  boot_diagnostics {
    storage_account_uri = azurerm_storage_account.storage-ctrl.primary_blob_endpoint  
  }

  network_interface_ids = [
    azurerm_network_interface.ehealth-ctrl-nic.id  
  ]

  os_disk {
    name                 = "ehealthOsCtrlNodeDisk"
    caching              = "ReadWrite"
    storage_account_type = "Premium_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  tags = {
    envireonment = "All"
  }

  depends_on = [ azurerm_network_interface.ehealth-ctrl-nic, tls_private_key.ehealth-ctrl-ssh ]
}

# DEV

resource "azurerm_network_security_group" "ehealth-dev-sec" {
  name                = "ehealthSecurityGroupDev"
  resource_group_name = azurerm_resource_group.ehealth.name    
  location            = azurerm_resource_group.ehealth.location

  security_rule {
    name                       = "ssh-sec"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
    environment = "Dev"
  }
}

resource "azurerm_public_ip" "ehealth-dev-public-ip" {
  name                = var.public_ip_dev
  resource_group_name = azurerm_resource_group.ehealth.name
  location            = azurerm_resource_group.ehealth.location
  allocation_method   = "Dynamic"

  tags = {
    envireonment = "Dev"
  }
}

resource "azurerm_network_interface" "ehealth-dev-nic" {
  name                = "ehealth-dev-nic"
  resource_group_name = azurerm_resource_group.ehealth.name
  location            = azurerm_resource_group.ehealth.location

  ip_configuration {
    name                          = "ehealth-dev-nic-config"
    subnet_id                     = module.vnet-dev.vnet_subnets[0]  //azurerm_subnet.xxxxxxxx.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.ehealth-dev-public-ip.id  
  }
}

data "azurerm_public_ip" "ehealth-dev-public-ip" {
  name                = azurerm_public_ip.ehealth-dev-public-ip.name 
  resource_group_name = azurerm_linux_virtual_machine.linux-dev.resource_group_name
}

resource "random_id" "randomid-dev" {
  keepers = {
    resource_group = azurerm_resource_group.ehealth.name
  }

  byte_length = 8
}

resource "azurerm_storage_account" "storage-dev" {
  name                     = "diag${random_id.randomid-dev.hex}"
  resource_group_name      = azurerm_resource_group.ehealth.name
  location                 = azurerm_resource_group.ehealth.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "tls_private_key" "ehealth-dev-ssh" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "export_private_dev_key" {
  content    = tls_private_key.ehealth-dev-ssh.private_key_pem 
  filename   = "id_rsa_dev.pem"
}

resource "local_file" "export_public_dev_key" {
  content    = tls_private_key.ehealth-dev-ssh.public_key_openssh
  filename   = "public_dev.pub"
}

resource "azurerm_network_interface_security_group_association" "ehealth_sga_dev" {
  network_interface_id      = azurerm_network_interface.ehealth-dev-nic.id  
  network_security_group_id = azurerm_network_security_group.ehealth-dev-sec.id
}

resource "azurerm_linux_virtual_machine" "linux-dev" {
  name                = var.linux_virtual_machine_dev_name
  resource_group_name = azurerm_resource_group.ehealth.name  
  location            = azurerm_resource_group.ehealth.location
  size                = "Standard_DS1_V2"

  computer_name                   = "ehealth-dev"
  admin_username                  = "azureuser"
  admin_password                  = "p@ssw0rd"
  disable_password_authentication = true

  admin_ssh_key {
    username   = "azureuser"
    public_key = tls_private_key.ehealth-dev-ssh.public_key_openssh 
  }

  boot_diagnostics {
    storage_account_uri = azurerm_storage_account.storage-dev.primary_blob_endpoint  
  }

  network_interface_ids = [
    azurerm_network_interface.ehealth-dev-nic.id  
  ]

  os_disk {
    name                 = "ehealthOsDevDisk"
    caching              = "ReadWrite"
    storage_account_type = "Premium_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  tags = {
    envireonment = "Dev"
  }
  depends_on = [ azurerm_network_interface.ehealth-dev-nic, tls_private_key.ehealth-dev-ssh ]
}

# UAT

resource "azurerm_network_security_group" "ehealth-uat-sec" {
  name                = "ehealthSecurityGroupUat"
  resource_group_name = azurerm_resource_group.ehealth.name    
  location            = azurerm_resource_group.ehealth.location

  security_rule {
    name                       = "ssh-sec"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
    environment = "Uat"
  }
}

resource "azurerm_public_ip" "ehealth-uat-public-ip" {
  name                = var.public_ip_uat
  resource_group_name = azurerm_resource_group.ehealth.name
  location            = azurerm_resource_group.ehealth.location
  allocation_method   = "Dynamic"

  tags = {
    envireonment = "Uat"
  }
}

resource "azurerm_network_interface" "ehealth-uat-nic" {
  name                = "ehealth-uat-nic"
  resource_group_name = azurerm_resource_group.ehealth.name
  location            = azurerm_resource_group.ehealth.location

  ip_configuration {
    name                          = "ehealth-uat-nic-config"
    subnet_id                     = module.vnet-uat.vnet_subnets[0]  //azurerm_subnet.xxxxxxxx.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.ehealth-uat-public-ip.id  
  }
}

data "azurerm_public_ip" "ehealth-uat-public-ip" {
  name                = azurerm_public_ip.ehealth-uat-public-ip.name
  resource_group_name = azurerm_linux_virtual_machine.linux-uat.resource_group_name
}

resource "random_id" "randomid-uat" {
  keepers = {
    resource_group = azurerm_resource_group.ehealth.name
  }

  byte_length = 8
}

resource "azurerm_storage_account" "storage-uat" {
  name                     = "diag${random_id.randomid-uat.hex}"
  resource_group_name      = azurerm_resource_group.ehealth.name
  location                 = azurerm_resource_group.ehealth.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "tls_private_key" "ehealth-uat-ssh" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "export_private_uat_key" {
  content    = tls_private_key.ehealth-uat-ssh.private_key_pem 
  filename   = "id_rsa_uat.pem"
}

resource "local_file" "export_public_uat_key" {
  content    = tls_private_key.ehealth-uat-ssh.public_key_openssh
  filename   = "public_uat.pub"
}

resource "azurerm_network_interface_security_group_association" "ehealth_sga_uat" {
  network_interface_id      = azurerm_network_interface.ehealth-uat-nic.id  
  network_security_group_id = azurerm_network_security_group.ehealth-uat-sec.id
}

resource "azurerm_linux_virtual_machine" "linux-uat" {
  name                = var.linux_virtual_machine_uat_name
  resource_group_name = azurerm_resource_group.ehealth.name  
  location            = azurerm_resource_group.ehealth.location
  size                = "Standard_DS1_V2"

  computer_name                   = "ehealth-uat-node"
  admin_username                  = "azureuser"
  admin_password                  = "p@ssw0rd"
  disable_password_authentication = true

  admin_ssh_key {
    username   = "azureuser"
    public_key = tls_private_key.ehealth-uat-ssh.public_key_openssh 
  }

  boot_diagnostics {
    storage_account_uri = azurerm_storage_account.storage-uat.primary_blob_endpoint  
  }

  network_interface_ids = [
    azurerm_network_interface.ehealth-uat-nic.id  
  ]

  os_disk {
    name                 = "ehealthOsUatDisk"
    caching              = "ReadWrite"
    storage_account_type = "Premium_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  tags = {
    envireonment = "Uat"
  }

  depends_on = [ azurerm_network_interface.ehealth-uat-nic, tls_private_key.ehealth-uat-ssh ]
}

# UAT - Selenium

resource "azurerm_network_security_group" "ehealth-uat-selenium-sec" {
  name                = "ehealthSecurityGroupUatSelenium"
  resource_group_name = azurerm_resource_group.ehealth.name    
  location            = azurerm_resource_group.ehealth.location

  security_rule {
    name                       = "ssh-sec"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
    environment = "Uat"
  }
}

resource "azurerm_public_ip" "ehealth-uat-selenium-public-ip" {
  name                = var.public_ip_uat-selenium
  resource_group_name = azurerm_resource_group.ehealth.name
  location            = azurerm_resource_group.ehealth.location
  allocation_method   = "Dynamic"

  tags = {
    envireonment = "Uat"
  }
}

resource "azurerm_network_interface" "ehealth-uat-selenium-nic" {
  name                = "ehealth-uat-selenium-nic"
  resource_group_name = azurerm_resource_group.ehealth.name
  location            = azurerm_resource_group.ehealth.location

  ip_configuration {
    name                          = "ehealth-uat-selenium-nic-config"
    subnet_id                     = module.vnet-uat.vnet_subnets[1]  //azurerm_subnet.xxxxxxxx.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.ehealth-uat-selenium-public-ip.id  
  }
}

data "azurerm_public_ip" "ehealth-uat-selenium-public-ip" {
  name                = azurerm_public_ip.ehealth-uat-selenium-public-ip.name
  resource_group_name = azurerm_windows_virtual_machine.windows-uat.resource_group_name
}

resource "random_id" "randomid-uat-selenium" {
  keepers = {
    resource_group = azurerm_resource_group.ehealth.name
  }

  byte_length = 8
}

resource "azurerm_storage_account" "storage-uat-selenium" {
  name                     = "diag${random_id.randomid-uat-selenium.hex}"
  resource_group_name      = azurerm_resource_group.ehealth.name
  location                 = azurerm_resource_group.ehealth.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "tls_private_key" "ehealth-uat-selenium-ssh" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "export_private_uat_selenium_key" {
  content    = tls_private_key.ehealth-uat-selenium-ssh.private_key_pem 
  filename   = "id_rsa_uat-selenium.pem"
}

resource "local_file" "export_public_uat-selenium_key" {
  content    = tls_private_key.ehealth-uat-selenium-ssh.public_key_openssh
  filename   = "public_uat-selenium.pub"
}

resource "azurerm_network_interface_security_group_association" "ehealth_sga_uat_selenium" {
  network_interface_id      = azurerm_network_interface.ehealth-uat-selenium-nic.id  
  network_security_group_id = azurerm_network_security_group.ehealth-uat-selenium-sec.id
}

resource "azurerm_windows_virtual_machine" "windows-uat" {
  name                = var.windows_virtual_machine_uat_name
  resource_group_name = azurerm_resource_group.ehealth.name  
  location            = azurerm_resource_group.ehealth.location
  size = "Standard_F2"

  computer_name                   = "ehealth-uat-sel"
  admin_username = "adminuser"
  admin_password = "p@ssw0rd1234!"

    network_interface_ids = [
        azurerm_network_interface.ehealth-uat-selenium-nic.id,
    ] 

  boot_diagnostics {
    storage_account_uri = azurerm_storage_account.storage-uat-selenium.primary_blob_endpoint  
  }

  os_disk {
        caching = "ReadWrite"
        storage_account_type = "Standard_LRS"
    }

    source_image_reference {
        publisher = "MicrosoftWindowsServer"
        offer = "WindowsServer"
        sku = "2016-Datacenter"
        version = "latest"  
    }

  tags = {
    envireonment = "Uat"
  }

  depends_on = [ azurerm_network_interface.ehealth-uat-selenium-nic, tls_private_key.ehealth-uat-selenium-ssh ]
}

# PROD

resource "azurerm_network_security_group" "ehealth-prod-sec" {
  name                = "ehealthSecurityGroupProd"
  resource_group_name = azurerm_resource_group.ehealth.name    
  location            = azurerm_resource_group.ehealth.location

  security_rule {
    name                       = "ssh-sec"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
    environment = "Prod"
  }
}

resource "azurerm_public_ip" "ehealth-prod-public-ip" {
  name                = var.public_ip_prod
  resource_group_name = azurerm_resource_group.ehealth.name
  location            = azurerm_resource_group.ehealth.location
  allocation_method   = "Dynamic"

  tags = {
    envireonment = "Prod"
  }
}

resource "azurerm_network_interface" "ehealth-prod-nic" {
  name                = "ehealth-prod-nic"
  resource_group_name = azurerm_resource_group.ehealth.name
  location            = azurerm_resource_group.ehealth.location

  ip_configuration {
    name                          = "ehealth-prod-nic-config"
    subnet_id                     = module.vnet-prod.vnet_subnets[0]  //azurerm_subnet.xxxxxxxx.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.ehealth-prod-public-ip.id  
  }
}

data "azurerm_public_ip" "ehealth-prod-public-ip" {
  name                = azurerm_public_ip.ehealth-prod-public-ip.name 
  resource_group_name = azurerm_linux_virtual_machine.linux-prod.resource_group_name
}

resource "random_id" "randomid-prod" {
  keepers = {
    resource_group = azurerm_resource_group.ehealth.name
  }

  byte_length = 8
}

resource "azurerm_storage_account" "storage-prod" {
  name                     = "diag${random_id.randomid-prod.hex}"
  resource_group_name      = azurerm_resource_group.ehealth.name
  location                 = azurerm_resource_group.ehealth.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "tls_private_key" "ehealth-prod-ssh" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "export_private_prod_key" {
  content    = tls_private_key.ehealth-prod-ssh.private_key_pem 
  filename   = "id_rsa_prod.pem"
}

resource "local_file" "export_public_prod_key" {
  content    = tls_private_key.ehealth-prod-ssh.public_key_openssh
  filename   = "public_prod.pub"
}

resource "azurerm_network_interface_security_group_association" "ehealth_sga_prod" {
  network_interface_id      = azurerm_network_interface.ehealth-prod-nic.id  
  network_security_group_id = azurerm_network_security_group.ehealth-prod-sec.id
}

resource "azurerm_linux_virtual_machine" "linux-prod" {
  name                = var.linux_virtual_machine_prod_name
  resource_group_name = azurerm_resource_group.ehealth.name  
  location            = azurerm_resource_group.ehealth.location
  size                = "Standard_DS1_V2"

  computer_name                   = "ehealth-prod"
  admin_username                  = "azureuser"
  admin_password                  = "p@ssw0rd"
  disable_password_authentication = true

  admin_ssh_key {
    username   = "azureuser"
    public_key = tls_private_key.ehealth-prod-ssh.public_key_openssh 
  }

  boot_diagnostics {
    storage_account_uri = azurerm_storage_account.storage-prod.primary_blob_endpoint  
  }

  network_interface_ids = [
    azurerm_network_interface.ehealth-prod-nic.id  
  ]

  os_disk {
    name                 = "ehealthOsProdDisk"
    caching              = "ReadWrite"
    storage_account_type = "Premium_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  tags = {
    envireonment = "Prod"
  }

  depends_on = [ azurerm_network_interface.ehealth-prod-nic, tls_private_key.ehealth-prod-ssh ]
}

# PROD MONITORING

resource "azurerm_network_security_group" "ehealth-prod-monitoring-sec" {
  name                = "ehealthSecurityGroupProdMonitoring"
  resource_group_name = azurerm_resource_group.ehealth.name    
  location            = azurerm_resource_group.ehealth.location

  security_rule {
    name                       = "ssh-sec"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
    environment = "Prod"
  }
}

resource "azurerm_public_ip" "ehealth-prod-monitoring-public-ip" {
  name                =  var.public_ip_prod-monitoring
  resource_group_name = azurerm_resource_group.ehealth.name
  location            = azurerm_resource_group.ehealth.location
  allocation_method   = "Dynamic"

  tags = {
    envireonment = "Prod"
  }
}

resource "azurerm_network_interface" "ehealth-prod-monitoring-nic" {
  name                = "ehealth-prod-monitoring-nic"
  resource_group_name = azurerm_resource_group.ehealth.name
  location            = azurerm_resource_group.ehealth.location

  ip_configuration {
    name                          = "ehealth-prod-monitoring-nic-config"
    subnet_id                     = module.vnet-prod.vnet_subnets[1]  //azurerm_subnet.xxxxxxxx.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.ehealth-prod-monitoring-public-ip.id  
  }
}

data "azurerm_public_ip" "ehealth-prod-monitoring-public-ip" {
  name                = azurerm_public_ip.ehealth-prod-monitoring-public-ip.name 
  resource_group_name = azurerm_linux_virtual_machine.linux-prod-monitoring.resource_group_name
}

resource "random_id" "randomid-prod-monitoring" {
  keepers = {
    resource_group = azurerm_resource_group.ehealth.name
  }

  byte_length = 8
}

resource "azurerm_storage_account" "storage-prod-monitoring" {
  name                     = "diag${random_id.randomid-prod-monitoring.hex}"
  resource_group_name      = azurerm_resource_group.ehealth.name
  location                 = azurerm_resource_group.ehealth.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "tls_private_key" "ehealth-prod-monitoring-ssh" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "export_private_prod_monitoring_key" {
  content    = tls_private_key.ehealth-prod-monitoring-ssh.private_key_pem 
  filename   = "id_rsa_prodmonitoring.pem"
}

resource "local_file" "export_public_prod_monitoring_key" {
  content    = tls_private_key.ehealth-prod-monitoring-ssh.public_key_openssh
  filename   = "public_prodmonitoring.pub"
}

resource "azurerm_network_interface_security_group_association" "ehealth_sga_prod_monitoring" {
  network_interface_id      = azurerm_network_interface.ehealth-prod-monitoring-nic.id  
  network_security_group_id = azurerm_network_security_group.ehealth-prod-monitoring-sec.id
}

resource "azurerm_linux_virtual_machine" "linux-prod-monitoring" {
  name                = var.linux_virtual_machine_prod_monitoring_name
  resource_group_name = azurerm_resource_group.ehealth.name  
  location            = azurerm_resource_group.ehealth.location
  size                = "Standard_DS1_V2"

  computer_name                   = "ehealth-prodmon"
  admin_username                  = "azureuser"
  admin_password                  = "p@ssw0rd"
  disable_password_authentication = true

  admin_ssh_key {
    username   = "azureuser"
    public_key = tls_private_key.ehealth-prod-monitoring-ssh.public_key_openssh 
  }

  boot_diagnostics {
    storage_account_uri = azurerm_storage_account.storage-prod-monitoring.primary_blob_endpoint  
  }

  network_interface_ids = [
    azurerm_network_interface.ehealth-prod-monitoring-nic.id  
  ]

  os_disk {
    name                 = "ehealthOsProdMonitoringDisk"
    caching              = "ReadWrite"
    storage_account_type = "Premium_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  tags = {
    envireonment = "Prod"
  }

  depends_on = [ azurerm_network_interface.ehealth-prod-monitoring-nic, tls_private_key.ehealth-prod-monitoring-ssh ]
}

# Install on ctrl

resource "null_resource" mysql_playbook {
  connection {
      type        = "ssh"
      user        = "azureuser"
      password    = "p@ssw0rd"
      private_key =  tls_private_key.ehealth-ctrl-ssh.private_key_openssh 
      host        = azurerm_linux_virtual_machine.linux-control-node.public_ip_address
    }

  provisioner "file" {
    source      = "./mysql_playbook.yml"
    destination = "mysql_playbook.yml"
  }

   depends_on = [ azurerm_linux_virtual_machine.linux-control-node ]
}

resource "null_resource" zap_owasp_playbook {
  connection {
      type        = "ssh"
      user        = "azureuser"
      password    = "p@ssw0rd"
      private_key =  tls_private_key.ehealth-ctrl-ssh.private_key_openssh 
      host        = azurerm_linux_virtual_machine.linux-control-node.public_ip_address
    }

  provisioner "file" {
    source      = "./zap_owasp_playbook.yml"
    destination = "zap_owasp_playbook.yml"
  }
   depends_on = [ azurerm_linux_virtual_machine.linux-control-node ]
}


resource "null_resource" donet_playbook {
  connection {
      type        = "ssh"
      user        = "azureuser"
      password    = "p@ssw0rd"
      private_key =  tls_private_key.ehealth-ctrl-ssh.private_key_openssh 
      host        = azurerm_linux_virtual_machine.linux-control-node.public_ip_address
    }

  provisioner "file" {
    source      = "./donet_playbook.yml"
    destination = "donet_playbook.yml"
  }
   depends_on = [ azurerm_linux_virtual_machine.linux-control-node ]
}

resource "null_resource" "install_ansible" {
  triggers = {
    controlnode_id = azurerm_linux_virtual_machine.linux-control-node.id
  }
  
// Déclarer un remote provisioner ici pour installer ansibel
  // https://medium.com/@yoursshaan2212/terraform-remote-exec-provisioner-with-azure-df78dae9229d
  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = "azureuser"
      password    = "p@ssw0rd"
      private_key =  tls_private_key.ehealth-ctrl-ssh.private_key_openssh 
      host        = azurerm_linux_virtual_machine.linux-control-node.public_ip_address
    }

    # Fichier containant le mot de passe initial de jenkins
    # /var/lib/jenkins/secrets/initialAdminPassword

    inline = [
      "sudo apt update && sudo apt install -y ansible",
      "sudo chmod -R 700 /home/azureuser/.ansible",
      "sudo chown -R azureuser:azureuser /home/azureuser/.ansible",
      
      
      "echo '${tls_private_key.ehealth-ctrl-ssh.private_key_openssh}' >> /home/azureuser/.ssh/id_rsa",
      "sudo chmod 600 /home/azureuser/.ssh/id_rsa",

      //"echo '${tls_private_key.my-ctrl-ssh.public_key_openssh} ${azurerm_linux_virtual_machine.linux-control-node.admin_username}@${azurerm_linux_virtual_machine.linux-control-node.computer_name}' >> /home/azureuser/.ssh/id_rsa.pub",
      //"echo '${tls_private_key.my-ctrl-ssh.public_key_openssh}' >> /home/azureuser/.ssh/id_rsa.pub",
      "echo '${chomp(tls_private_key.ehealth-ctrl-ssh.public_key_openssh)} ${azurerm_linux_virtual_machine.linux-control-node.admin_username}@${azurerm_linux_virtual_machine.linux-control-node.computer_name}' >> /home/azureuser/.ssh/id_rsa.pub",
      "sudo chmod 600 /home/azureuser/.ssh/id_rsa.pub",
      
      "sudo apt update",
      "sudo apt -y install openjdk-17-jdk",
      "sudo wget -O /usr/share/keyrings/jenkins-keyring.asc https://pkg.jenkins.io/debian-stable/jenkins.io-2023.key",
      "sudo echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] https://pkg.jenkins.io/debian-stable binary/ | sudo tee /etc/apt/sources.list.d/jenkins.list > /dev/null",

      "sudo apt-get update",
      "wget http://ftp.kr.debian.org/debian/pool/main/i/init-system-helpers/init-system-helpers_1.60_all.deb",
      "sudo apt install ./init-system-helpers_1.60_all.deb",
      "sudo apt-get install jenkins -y",

      "sudo sh -c 'cat /var/lib/jenkins/secrets/initialAdminPassword | tee jenflo.key'",

      "sudo apt update",
      "sudo apt -y install maven",

//https://medium.com/@kelom.x/ansible-mysql-installation-2513d0f70faf#id_token=eyJhbGciOiJSUzI1NiIsImtpZCI6IjkzYjQ5NTE2MmFmMGM4N2NjN2E1MTY4NjI5NDA5NzA0MGRhZjNiNDMiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhenAiOiIyMTYyOTYwMzU4MzQtazFrNnFlMDYwczJ0cDJhMmphbTRsamRjbXMwMHN0dGcuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiIyMTYyOTYwMzU4MzQtazFrNnFlMDYwczJ0cDJhMmphbTRsamRjbXMwMHN0dGcuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMDY3OTI5MzU1NDYyNjE3MDM5NTIiLCJlbWFpbCI6ImZsb3JlbnQubmtlbmRhQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJuYmYiOjE3MTI3MzIzMzEsIm5hbWUiOiJGbG9yZW50IE5rZW5kYSIsInBpY3R1cmUiOiJodHRwczovL2xoMy5nb29nbGV1c2VyY29udGVudC5jb20vYS9BQ2c4b2NKc2xMWU00Si16MllNTmlqeGoxOUhGTHMwSk5QbXpNVmU0eVFlUVFqa3MtWHRGaS1FPXM5Ni1jIiwiZ2l2ZW5fbmFtZSI6IkZsb3JlbnQiLCJmYW1pbHlfbmFtZSI6Ik5rZW5kYSIsImlhdCI6MTcxMjczMjYzMSwiZXhwIjoxNzEyNzM2MjMxLCJqdGkiOiJhY2MyZDBiMmI4ODZjOWY3NjQyNWEzZDY0YzE1NTBkNjllN2RhMjMxIn0.MuoBhAauNzcGTb6HCra7tf0Vshi-dLFTEpKP0ilABtpNJeE10VHTlSZajmHrs5yzLqkBVZhwhC02tq0IAziY6wGbyZVtrRT_YU5UTjZK7IjGDFrrowoatpmWKIudtmudmjIU4iCREaf11cARTQD3JsNl9QFkElyTVMHj8BVJNhZkLbcxrW71CjmF8w0rPtCsTykXGRsxelK_XGH7MdoNmd_OllwmfvGktYJ38SLIIZ5l3tycp3gUfsEu2X8K1eEh-ncTq5mJ2f5Puc7Wwq0N3yK8dYlD3Yg9S7V-CKUUKbVXedcwPFGpr8451PSQY_h3bzXTxSsVBbtf5VMrNUAEaw
//db1 ansible_ssh_user=node ansible_ssh_host=192.168.20.146
//db2 ansible_ssh_user=node ansible_ssh_host=192.168.20.147

      "sudo sh -c  'echo '[Dbservers]' >> /etc/ansible/hosts'",
      "sudo sh -c  'echo '${azurerm_linux_virtual_machine.linux-dev.private_ip_address}' >> /etc/ansible/hosts'",

      //"rm -f group_vars",
      "mkdir group_vars",
      "sudo sh -c \"sed '/^\\[defaults\\]/a\\host_key_checking = False' /etc/ansible/ansible.cfg > modcfg && mv modcfg /etc/ansible/ansible.cfg\"",
    ]
  }

  depends_on = [ azurerm_linux_virtual_machine.linux-dev, azurerm_linux_virtual_machine.linux-control-node ]
}

resource "null_resource" "run_mysql_playbook" {
  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = "azureuser"
      password    = "p@ssw0rd"
      private_key =  tls_private_key.ehealth-ctrl-ssh.private_key_openssh 
      host        = azurerm_linux_virtual_machine.linux-control-node.public_ip_address
    }
  inline = [
    // Install MySql in the target1 machine using Ansible
    "ansible-playbook mysql_playbook.yml"
   ]
  }
  depends_on = [ null_resource.install_ansible, null_resource.install_dev, null_resource.playbook_group_vars_all ]
}

resource "null_resource" playbook_group_vars_all {
 connection {
      type        = "ssh"
      user        = "azureuser"
      password    = "p@ssw0rd"
      private_key =  tls_private_key.ehealth-ctrl-ssh.private_key_openssh 
      host        = azurerm_linux_virtual_machine.linux-control-node.public_ip_address
    }

  provisioner "file" {
    source      = "./all"
    destination = "group_vars/all"
  }

   depends_on = [ azurerm_linux_virtual_machine.linux-control-node, null_resource.install_ansible ]
}

# resource "local_file" "export_jenkins_key" {
#   source = "jenflo.key"
#   filename   = "jenkins.key"
#   connection {
#       type        = "ssh"
#       user        = "azureuser"
#       password    = "p@ssw0rd"
#       private_key =  tls_private_key.my-ctrl-ssh.private_key_openssh 
#       host        = azurerm_linux_virtual_machine.linux-control-node.public_ip_address
#     }

#    depends_on = [ azurerm_linux_virtual_machine.linux-control-node, null_resource.install_ansible ]
# }

#####################################################
# Install Dev
#####################################################

resource "null_resource" "install_dev" {
 triggers = {
  id = azurerm_linux_virtual_machine.linux-dev.id
 }
 provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = "azureuser"
      password    = "p@ssw0rd"
      private_key =  tls_private_key.ehealth-dev-ssh.private_key_openssh
      host        =  azurerm_linux_virtual_machine.linux-dev.public_ip_address  
    }

    inline = [

      
      "echo '${tls_private_key.ehealth-dev-ssh.private_key_openssh}' >> /home/azureuser/.ssh/id_rsa",
      "sudo chmod 600 /home/azureuser/.ssh/id_rsa",

      "echo '${tls_private_key.ehealth-dev-ssh.public_key_openssh}' >> /home/azureuser/.ssh/id_rsa.pub",
      "sudo chmod 600 /home/azureuser/.ssh/id_rsa.pub",

      "echo '${chomp(tls_private_key.ehealth-ctrl-ssh.public_key_openssh)} ${azurerm_linux_virtual_machine.linux-control-node.admin_username}@${azurerm_linux_virtual_machine.linux-control-node.computer_name}' >> /home/azureuser/.ssh/authorized_keys",
      "sudo chmod 600 /home/azureuser/.ssh/authorized_keys",
      
      "sudo apt update",
      "sudo apt -y install openjdk-17-jdk",

      "sudo apt update",
      "sudo apt -y install maven",

      "echo 'export JAVA_HOME=/usr/lib/jvm/java-1.17.0-openjdk-amd64' > /etc/profile.d/maven.sh", 
      "echo 'export M2_HOME=/usr/share/maven' >> /etc/profile.d/maven.sh",
      "echo 'export MAVEN_HOME=/usr/share/maven' >> /etc/profile.d/maven.sh",
      "echo 'export PATH=$${M2_HOME}/bin:$${PATH}' >> /etc/profile.d/maven.sh",

      "sudo chmod +x /etc/profile.d/maven.sh",

      "sudo source /etc/profile.d/maven.sh",
      
      "sudo apt update",
      "sudo apt -y install python-pip"
    ]
  }
 depends_on = [ tls_private_key.ehealth-dev-ssh, tls_private_key.ehealth-ctrl-ssh, azurerm_linux_virtual_machine.linux-control-node, azurerm_linux_virtual_machine.linux-dev, azurerm_network_interface_security_group_association.ehealth_sga_ctrl ]
}