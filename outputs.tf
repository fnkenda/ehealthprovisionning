#############################################################################
# OUTPUTS
#############################################################################

output "vnet_controlnode_id" {
  value = module.vnet-controlnode.vnet_id
}

output "vnet_dev_id" {
  value = module.vnet-dev.vnet_id
}

output "vnet_uat_id" {
  value = module.vnet-uat.vnet_id
}

output "vnet_prod_id" {
  value = module.vnet-prod.vnet_id
}

#############################################################################
# OUTPUTS - SECURITY INFO
#############################################################################

# output "vnet_sec_id" {
#   value = module.vnet-sec.vnet_id
# }

# output "vnet_sec_name" {
#   value = module.vnet-sec.vnet_name
# }

# output "service_principal_client_id" {
#   value = azuread_service_principal.vnet_peering.id
# }

# output "service_principal_client_secret" {
#   value = nonsensitive(random_password.vnet_peering.result)
# }

# output "resource_group_name" {
#   value = var.sec_resource_group_name
# }