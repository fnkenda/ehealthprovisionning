#############################################################################
# VARIABLES
#############################################################################

# GLOBAL

variable "resource_group_name" {
  type = string
  default = "ehealth"
}

variable "location" {
  type    = string
  default = "centralus"
}

variable "sec_resource_group_name" {
  type = string
  default = "ehealth-sec"
}

variable "use_for_each" {
  type    = bool
  default = true
}

variable "costcenter" {
    type = string
    default = "us"
  
}

# CONTROL NODE

variable "vnet-controlnode-name" {
    type = string
    default = "vnet-controlnode"
}

variable "vnet_cidr_range_controlnode" {
  type    = string
  default = "10.0.0.0/16"
}

variable "subnet_prefixes_controlnode" {
  type    = list(string)
  default = ["10.0.0.0/24"]
}

variable "subnet_names_controlnode" {
  type    = list(string)
  default = ["controlnode"]
}


# DEVELOPMENT (DEV) 

variable "vnet-dev-name" {
    type = string
    default = "vnet-dev"
}

variable "vnet_cidr_range_dev" {
  type    = string
  default = "10.1.0.0/16"
}

variable "subnet_prefixes_dev" {
  type    = list(string)
  default = ["10.1.0.0/24"]
}

variable "subnet_names_dev" {
  type    = list(string)
  default = ["dev"]
}

# USER ACCEPTANCE TESTING (UAT) 

variable "vnet-uat-name" {
    type = string
    default = "vnet-uat"
}

variable "vnet_cidr_range_uat" {
  type    = string
  default = "10.2.0.0/16"
}

variable "subnet_prefixes_uat" {
  type    = list(string)
  default = ["10.2.0.0/24", "10.2.1.0/24"]
}

variable "subnet_names_uat" {
  type    = list(string)
  default = ["uat", "uat-selenium"]
}

# PRODUCTION (PROD) 

variable "vnet-prod-name" {
    type = string
    default = "vnet-prod"
}

variable "vnet_cidr_range_prod" {
  type    = string
  default = "10.3.0.0/16"
}

variable "subnet_prefixes_prod" {
  type    = list(string)
  default = ["10.3.0.0/24", "10.3.1.0/24"]
}

variable "subnet_names_prod" {
  type    = list(string)
  default = ["prod", "prod-monitoring"]
}

# SECURITY (SEC) 

# variable "security_environment_name" {
#   type = string
#   default = "security"
# }

# variable "vnet-sec-name" {
#     type = string
#     default = "vnet-sec"
# }

# variable "vnet_cidr_range_sec" {
#   type    = string
#   default = "10.4.0.0/16"
# }

# variable "subnet_prefixes_sec" {
#   type    = list(string)
#   default = ["10.4.0.0/24", "10.4.1.0/24"]
# }

# variable "subnet_names_sec" {
#   type    = list(string)
#   default = ["siem", "inspect"]
# }


variable linux_virtual_machine_control_node_name {
  type = string
}

variable linux_virtual_machine_dev_name {
  type = string
}

variable linux_virtual_machine_uat_name {
  type = string
}

variable windows_virtual_machine_uat_name {
  type = string
}

variable linux_virtual_machine_prod_name {
  type = string
}

variable linux_virtual_machine_prod_monitoring_name {
  type = string
}

variable public_ip_ctrl {
  type = string
}

variable public_ip_dev {
  type = string
}

variable public_ip_uat {
  type = string
}

variable public_ip_uat-selenium {
  type = string
}


variable public_ip_prod {
  type = string
}

variable public_ip_prod-monitoring {
  type = string
}